package ru.easton.tm.controller;

import ru.easton.tm.entity.User;
import ru.easton.tm.service.UserService;

import java.util.List;

public class UserController extends AbstractController{

    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    public int createUser(){
        System.out.println("[CREATE USER]");
        System.out.println("PLEASE, ENTER LOGIN: ");
        final String login = scanner.nextLine();
        System.out.println("PLEASE, ENTER PASSWORD: ");
        final String password = scanner.nextLine();
        final User user = userService.create(login, password);
        if(user == null) System.out.println("FAIL");
        System.out.println("OK");
        return 0;
    }

    public int clearUser() {
        System.out.println("[CLEAR USER]");
        userService.clear();
        System.out.println("[OK]");
        return 0;
    }

    public int listUser() {
        System.out.println("[LIST USERS]");
        viewUsers(userService.findAll());
        System.out.println("[OK]");
        return 0;
    }

    private void viewUser(final User user){
        if(user == null) return;
        System.out.println("[VIEW USER]");
        System.out.println("ID: " + user.getId());
        System.out.println("LOGIN: " + user.getLogin());
        System.out.println("LAST NAME: " + user.getLastName());
        System.out.println("FIRST NAME: " + user.getFirstName());
        System.out.println("MIDDLE NAME: " + user.getMiddleName());
        System.out.println("[OK]");
    }

    public void viewUsers(final List<User> users){
        int index = 1;
        for(final User user: users){
            System.out.println(user.toString());
            index++;
        }
    }

    public int viewUserById() {
        System.out.println("[PLEASE, ENTER ID]");
        final Long id;
        try {
            id = Long.parseLong(scanner.nextLine());
        } catch (NumberFormatException e) {
            System.out.println("[ENTER NUMBER, NOT CHAR]");
            return 0;
        }
        final User user = userService.findById(id);
        if(user == null){
            System.out.println("[USER NOT FOUND]");
            return 0;
        }
        viewUser(user);
        return 0;
    }

    public int updateUserById() {
        System.out.println("[PLEASE, ENTER ID]");
        final Long id;
        try {
            id = Long.parseLong(scanner.nextLine());
        } catch (NumberFormatException e) {
            System.out.println("[ENTER NUMBER, NOT CHAR]");
            return 0;
        }
        System.out.println("[PLEASE, ENTER LAST NAME]");
        final String lastName = scanner.nextLine();
        System.out.println("[PLEASE, ENTER FIRST NAME]");
        final String firstName = scanner.nextLine();
        System.out.println("[PLEASE, ENTER MIDDLE NAME]");
        final String middleName = scanner.nextLine();
        final User user = userService.update(id, firstName, lastName, middleName);
        if(user == null) {
            System.out.println("[USER NOT FOUND]");
            return 0;
        }
        System.out.println("[OK]");
        return 0;
    }

    public int removeById() {
        System.out.println("[REMOVE USER BY ID]");
        System.out.println("PLEASE, ENTER USER ID: ");
        Long id = null;
        if(scanner.hasNextLong())
            id = scanner.nextLong();
        final User user = userService.removeById(id);
        if(user == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
        return 0;
    }
}
