package ru.easton.tm.controller;

public class SystemController {

    public int displayExit(){
        System.out.println("Terminate program.");
        return -1;
    }

    public int displayError(){
        System.out.println("Error! Unknown program argument.");
        return -1;
    }

    public void displayWelcome(){
        System.out.println("** WELCOME TO TASK MANAGER **");
    }

    public int displayHelp(){
        System.out.println("version - Display application version.");
        System.out.println("about - Display developer info.");
        System.out.println("help - Display list of commands.");
        System.out.println("exit - Terminate console application.");
        System.out.println();
        System.out.println("project-list - Display list of projects.");
        System.out.println("project-create - Create new project by name.");
        System.out.println("project-clear - Remove all projects.");
        System.out.println("project-view - View project by index.");
        System.out.println("project-remove-by-name - Remove project by name.");
        System.out.println("project-remove-by-id - Remove project by id.");
        System.out.println("project-remove-by-index - Remove project by index.");
        System.out.println("project-update-by-index - Update project by index.");
        System.out.println();
        System.out.println("task-list - Display list of tasks.");
        System.out.println("task-create - Create new task by name.");
        System.out.println("task-clear - Remove all tasks.");
        System.out.println("task-view - View task by index.");
        System.out.println("task-remove-by-name - Remove task by name.");
        System.out.println("task-remove-by-id - Remove task by id.");
        System.out.println("task-remove-by-index - Remove task by index.");
        System.out.println("task-update-by-index - Update task by index.");
        System.out.println("task_list_by_project_id - Display task list by project id.");
        System.out.println("task_add_to_project_by_id - Add task to project by id.");
        System.out.println("task_remove_from_project_by_id - Remove task from project by id.");
        System.out.println();
        System.out.println("user-list - Display list of users.");
        System.out.println("user-create - Create new user.");
        System.out.println("user-clear - Remove all users.");
        System.out.println("user-view - View user by id.");
        System.out.println("user-update - Update user by id.");
        System.out.println("user-remove_by_id - Remove user by id.");

        return 0;
    }

    public int  displayVersion(){
        System.out.println("1.0.0");
        return 0;
    }

    public int  displayAbout(){
        System.out.println("Pavel Afanasov");
        System.out.println("a.p.a.easton@gmail.com");
        return 0;
    }

}
