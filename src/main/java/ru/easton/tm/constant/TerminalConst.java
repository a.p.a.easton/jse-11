package ru.easton.tm.constant;

public class TerminalConst {

    public static final String HELP = "help";
    public static final String VERSION = "version";
    public static final String ABOUT = "about";
    public static final String EXIT = "exit";

    public static final String PROJECT_CREATE = "project-create";
    public static final String PROJECT_CLEAR = "project-clear";
    public static final String PROJECT_LIST = "project-list";
    public static final String PROJECT_VIEW = "project-view";
    public static final String PROJECT_REMOVE_BY_NAME = "project-remove-by-name";
    public static final String PROJECT_REMOVE_BY_ID = "project-remove-by-id";
    public static final String PROJECT_REMOVE_BY_INDEX = "project-remove-by-index";
    public static final String PROJECT_UPDATE_BY_INDEX = "project-update-by-index";

    public static final String TASK_CREATE = "task-create";
    public static final String TASK_CLEAR = "task-clear";
    public static final String TASK_LIST = "task-list";
    public static final String TASK_VIEW = "task-view";
    public static final String TASK_REMOVE_BY_NAME = "task-remove-by-name";
    public static final String TASK_REMOVE_BY_ID = "task-remove-by-id";
    public static final String TASK_REMOVE_BY_INDEX = "task-remove-by-index";
    public static final String TASK_UPDATE_BY_INDEX = "task-update-by-index";
    public static final String TASK_LIST_BY_PROJECT_ID = "task_list_by_project_id";
    public static final String TASK_ADD_TO_PROJECT_BY_ID = "task_add_to_project_by_id";
    public static final String TASK_REMOVE_FROM_PROJECT_BY_ID = "task_remove_from_project_by_id";

    public static final String USER_CREATE = "user-create";
    public static final String USER_CLEAR = "user-clear";
    public static final String USER_LIST = "user-list";
    public static final String USER_VIEW = "user-view";
    public static final String USER_UPDATE = "user-update";
    public static final String USER_REMOVE_BY_ID = "user-remove_by_id";
    public static final String USER_REMOVE_BY_INDEX = "user-remove_by_index";
    public static final String USER_REMOVE_BY_LOGIN = "user-remove_by_login";

}
