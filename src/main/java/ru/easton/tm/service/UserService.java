package ru.easton.tm.service;

import ru.easton.tm.entity.User;
import ru.easton.tm.enumerated.Role;
import ru.easton.tm.repository.UserRepository;
import ru.easton.tm.util.HashUtil;

import java.util.List;

public class UserService {

    private final UserRepository userRepository;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public User create(final String login, final String password) {
        if(login == null || login.isEmpty()) return null;
        if(password == null || password.isEmpty()) return null;
        if(existByLogin(login)) return null;
        final String passwordHash = HashUtil.md5(password);
        final User user = new User(login, passwordHash);
        userRepository.create(user);
        return user;
    }

    public User create(final String login, final String password, final Role role) {
        final User user = create(login, password);
        if(user == null) return null;
        user.setRole(role);
        return user;
    }

    public User update(final Long id, final String firstName, final String lastName, final String middleName) {
        final User user = findById(id);
        if(user == null) return null;
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        return user;
    }

    public void clear() {
        userRepository.clear();
    }

    public List<User> findAll() {
        return userRepository.findAll();
    }

    public User findByIndex(final int index) {
        if(index < 0) return null;
        return userRepository.findByIndex(index);
    }

    public User findById(final Long id) {
        if(id == null) return null;
        return userRepository.findById(id);
    }

    public User removeById(final Long id) {
        if(id == null) return null;
        return userRepository.removeById(id);
    }

    public User removeByIndex(final int index) {
        if(index < 0) return null;
        return userRepository.removeByIndex(index);
    }

    public boolean existByLogin(final String login){
        if(login == null || login.isEmpty()) return false;
        return userRepository.existByLogin(login);
    }

}
